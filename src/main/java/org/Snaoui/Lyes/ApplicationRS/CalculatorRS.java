package org.Snaoui.Lyes.ApplicationRS;

import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/calcul")
public class CalculatorRS {
	@GET
	@Path("add/{x}/{y}")
	public int add(@PathParam("x") int a,@PathParam("y") int b) {
		return a+b;
	}
	@GET
	@Path("bonjour/{x}")
	public String bonjour(@PathParam("x")int annee) {
		
		return "Bonjour "+annee + " !!!";
	}
}
