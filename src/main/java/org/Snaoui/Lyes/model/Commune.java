package org.Snaoui.Lyes.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Access(AccessType.FIELD)

public class Commune {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@XmlElement(name="Id")
	private  long Id;
	@Column(name="name", length=80)
	@XmlElement(name="Name")
	private String Name;

	public Commune() {
		// TODO Auto-generated constructor stub
	}
	public Commune(String Name) {
		this.Name= Name;
		
	}
	public Commune(long Id, String Name) {
		this.Id=Id;
		this.Name= Name;
		
	}

	public long getId() {
		return Id;
	}

	@Override
	public String toString() {
		return "Commune [Id=" + Id + ", Name=" + Name + "]";
	}

	public void setId(long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}
	

}
