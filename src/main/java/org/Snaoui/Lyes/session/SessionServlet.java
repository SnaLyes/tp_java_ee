package org.Snaoui.Lyes.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionServlet
 */
@WebServlet("/SessionServlet")
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private long count = 0;
    public SessionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		count++;
		//response.getWriter().append("Served at: ").append(request.getContextPath());
//		PrintWriter writer= response.getWriter();
//		writer.print("Resquest : "+count);
		
		HttpSession session= request.getSession();
		if (session.getAttribute("count")==null)
		{
			long count =1L;
			session.setAttribute("count",count);
		}
		else {
			Long count= (long)session.getAttribute("count");
			count++;
			session.setAttribute("count",count);
		}
		PrintWriter writer= response.getWriter();
		writer.print("Resquest : "+session.getAttribute("count"));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
